﻿#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctime>
#include <chrono>

const int Array = 10;

int main()
{
	int sumArray[Array][Array];

	for (int i = 0; i < Array; i++)
	{
		std::string result;
		for (int j = 0; j < Array; j++)
		{
			sumArray[i][j] = i + j;
			std::cout << sumArray[i][j] << "\t";
		}

		std::cout << result << std::endl;
	}

	time_t timeNow = time(0);
	tm* local_time = localtime(&timeNow);
	int day = local_time->tm_mday;

	std::cout << "Today is " << day << " day of month\n";

	for (int i = 0; i < Array; i++)
	{
		if (day % Array == i)
		{
			int sum = 0;

			for (int x = 0; x < Array; x++)
			{
				sum += sumArray[i][x];
			}

			std::cout << "Sum of values in the line with index " << i << " = " << sum << std::endl;
		}
	}
}